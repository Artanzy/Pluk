package com.example.tannatanon.pluk.Classes;

/**
 * Created by tannatanon on 6/4/2016 AD.
 */
public class Account {
    String username;
    String password;

    public Account(String username,String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }
}
