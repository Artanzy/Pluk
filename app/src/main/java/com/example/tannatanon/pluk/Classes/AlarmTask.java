package com.example.tannatanon.pluk.Classes;


/**
 * Created by tannatanon on 6/5/2016 AD.
 */
public class AlarmTask {
    int id;
    String name;
    String surname;
    String time;
    public AlarmTask(String name,String surname,String time){
        this.name = name;
        this.surname = surname;
        this.time = time;
    }

    public String getName(){
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public String getTime(){
        return time;
    }
}
