package com.example.tannatanon.pluk.Classes;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.tannatanon.pluk.R;
import com.example.tannatanon.pluk.activity.InAppActivity;

import java.util.List;

/**
 * Created by tannatanon on 6/5/2016 AD.
 */
public class TaskAdapter extends ArrayAdapter<AlarmTask> {
    public TaskAdapter(Context context, int resource, List<AlarmTask> tasks) {
        super(context, resource, tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;
        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.task_row,null);
        }

        final AlarmTask task = getItem(position);

        if(task != null){
            TextView name = (TextView) v.findViewById(R.id.name_text);
            name.setText(task.getName());
            TextView surname = (TextView) v.findViewById(R.id.surname_text);
            surname.setText(task.getSurname());
            TextView clock = (TextView) v.findViewById(R.id.textClock);
            clock.setText(task.getTime());
            Button delete = (Button) v.findViewById(R.id.delete_button);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<AlarmTask> alarmTaskList = DataManager.alarmTaskList;
                    alarmTaskList.remove(task);
                    Context context = getContext();
                    Intent intent = new Intent(context, InAppActivity.class);
                    context.startActivity(intent);
                }
            });
        }

        return v;
    }
}
