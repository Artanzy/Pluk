package com.example.tannatanon.pluk.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.tannatanon.pluk.Classes.Account;
import com.example.tannatanon.pluk.Classes.DataManager;
import com.example.tannatanon.pluk.R;

import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageView img_view;
    EditText userText;
    EditText passText;
    Button login;
    Button register;
    Account admin = new Account("admin","1234");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init(){
        img_view = (ImageView)findViewById(R.id.clock_img);
        userText = (EditText)findViewById(R.id.user_text);
        passText = (EditText)findViewById(R.id.pass_text);
        File imgFile = new File("/src/img/alarm_clock.png");
        if(imgFile.exists()) {
            img_view.setImageURI(Uri.fromFile(imgFile));
        }
        login = (Button) findViewById(R.id.login_button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkLogin(userText.getText() + "", passText.getText() + "", DataManager.accountList)){
                    Intent intent = new Intent(MainActivity.this, InAppActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        register = (Button) findViewById(R.id.register_button);
        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisActivity.class);
                startActivity(intent);
                finish();
            }
        });
        DataManager.accountList.add(admin);
    }

    public boolean checkLogin(String username,String password,List<Account> accountList){
        for(Account account : accountList){
            if(username.equals(account.getUsername()) && password.equals(account.getPassword())){
                return true;
            }
        }
        return false;
    }
}
