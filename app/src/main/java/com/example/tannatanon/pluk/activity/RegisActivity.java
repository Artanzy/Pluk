package com.example.tannatanon.pluk.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tannatanon.pluk.Classes.Account;
import com.example.tannatanon.pluk.Classes.DataManager;
import com.example.tannatanon.pluk.R;

/**
 * Created by tannatanon on 6/4/2016 AD.
 */
public class RegisActivity extends AppCompatActivity {

    EditText user_text;
    EditText pass_text;
    EditText conf_text;
    Button confirm;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis);
        init();
    }

    public void init(){
        user_text = (EditText)findViewById(R.id.user_edit);
        pass_text = (EditText)findViewById(R.id.pass_edit);
        conf_text = (EditText)findViewById(R.id.conf_edit);
        confirm = (Button) findViewById(R.id.confirm_button);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((pass_text.getText()+"").equals(conf_text.getText()+"")) {
                    Account account = new Account(user_text.getText() + "", pass_text.getText() + "");
                    DataManager.accountList.add(account);
                    Intent intent = new Intent(RegisActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
