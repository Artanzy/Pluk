package com.example.tannatanon.pluk.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.tannatanon.pluk.Classes.AlarmTask;
import com.example.tannatanon.pluk.Classes.DataManager;
import com.example.tannatanon.pluk.R;

import java.util.List;

/**
 * Created by tannatanon on 6/5/2016 AD.
 */
public class EditActivity extends AppCompatActivity {

    Button confirm;
    EditText nameText;
    EditText surnameText;
    TimePicker timePicker;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        init();
    }

    public void init(){
        confirm = (Button) findViewById(R.id.edit_confirm_button);
        nameText = (EditText) findViewById(R.id.name_editText);
        surnameText = (EditText) findViewById(R.id.surname_editText);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmTask task;
                if(timePicker.is24HourView()) {
                    task = new AlarmTask(nameText.getText() + "",
                            surnameText.getText() + "",
                            String.format("%02d:%02d",(timePicker.getHour()+12),timePicker.getMinute()));
                }
                else {
                    task = new AlarmTask(nameText.getText() + "",
                            surnameText.getText() + "",
                            String.format("%02d:%02d",timePicker.getHour(),timePicker.getMinute()));
                }
                List<AlarmTask> alarmTaskList = DataManager.alarmTaskList;
                alarmTaskList.add(task);
                Intent intent = new Intent(EditActivity.this,InAppActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
