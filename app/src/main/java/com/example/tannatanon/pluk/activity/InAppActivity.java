package com.example.tannatanon.pluk.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.tannatanon.pluk.Classes.AlarmTask;
import com.example.tannatanon.pluk.Classes.DataManager;
import com.example.tannatanon.pluk.Classes.TaskAdapter;
import com.example.tannatanon.pluk.R;

import java.util.List;

/**
 * Created by tannatanon on 6/5/2016 AD.
 */
public class InAppActivity extends AppCompatActivity {

    List<AlarmTask> taskList;
    TaskAdapter taskAdapter;
    ListView taskListView;
    Button addbutton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inapp);
        init();
    }

    public void init(){
        taskList = DataManager.alarmTaskList;
        taskListView = (ListView) findViewById(R.id.task_listView);
        taskAdapter = new TaskAdapter(this,R.layout.task_row,taskList);
        taskListView.setAdapter(taskAdapter);
        addbutton = (Button) findViewById(R.id.add_button);
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InAppActivity.this,EditActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
